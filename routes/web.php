<?php


use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\registrationcontroller;
use App\Http\Controllers\indexcontroller;
use App\Http\Controllers\PostController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('example',[indexcontroller::class,'indi']);

Auth::routes();

Route::get('/',[indexcontroller::class,'index']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/post/create', [App\Http\Controllers\PostController::class, 'create'])->name('post.create');
Route::post('/post/store', [App\Http\Controllers\PostController::class, 'store'])->name('post.store');

Route::get('/posts', [App\Http\Controllers\PostController::class, 'index'])->name('posts');

Route::get('/post/show/{id}', [App\Http\Controllers\PostController::class, 'show'])->name('post.show');

Route::post('/comment/store', [App\Http\Controllers\PostController::class, 'store'])->name('comment.add');

