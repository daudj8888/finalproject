<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <script src="{{ asset('js/main.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->

    <link href="{{ asset('css/logincss/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/logincss/util.css') }}" rel="stylesheet">


</head>
<body background>
<div class="limiter">

    <div class="container-login100">

        <div class="wrap-login100">

            <form method="POST" class="login100-form validate-form" action="{{ route('login') }}">
                @csrf

                <span class="login100-form-title p-b-26">
                    Login Here
                </span>

                <span class="login100-form-title p-b-48">
                    <i class="zmdi zmdi-font"></i>
                </span>

                <div class="wrap-input100 validate-input">
                     <input id="email" type="email" class="input100 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required
                     autocomplete="email" autofocus>
                     <span class="focus-input100" data-placeholder="Email"></span>
                     @error('email')
                     <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="wrap-input100 validate-input">
                    <span class="btn-show-pass">
                        <i class="zmdi zmdi-eye"></i>
                    </span>
                    <input id="password" type="password" class="input100 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    <span class="focus-input100" data-placeholder="Password"></span>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="container-login100-form-btn">

                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button type="submit" class="login100-form-btn">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>

                        @if (Route::has('password.request'))
                        <div style="text-align: center;padding:5px;" >
                            <a   href="{{ route('password.request') }}"  style="">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        </div>
                        @endif
                        <div class="text-center p-t-115">
                            <span class="txt1">
                               <h3> Don’t have an account?</h3>
                            </span>

                            <a class="txt2" href="{{url('/register')}}">
                                <h4>Sign Up</h4>
                            </a>
                        </div>
             </form>
        </div>
    </div>
</div>


</body>
</html>
