<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <script src="{{ asset('js/main.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/logincss/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/logincss/util.css') }}" rel="stylesheet">


</head>
<body>

<div class="limiter">

    <section class="container-login100">

        <div class="wrap-login100">






                <form action="{{ route('register')}}" method="POST" id="signup-form" class="login100-form validate-form" >
                    @csrf
                    <span style=" padding-bottom:40px;"  class="login100-form-title p-b-26">
                        Create an Account
                    </span>



                            <div class="wrap-input100 validate-input">
                                <input id="name" type="text" class="input100 @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                <span class="focus-input100" data-placeholder="Your Name"></span>
                                @error('name')
                                    <span  role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="wrap-input100 validate-input">
                                <input id="email"  type="email" class="input100 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                <span class="focus-input100" data-placeholder="Your Email"></span>
                                @error('email')
                                    <span  role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="wrap-input100 validate-input">
                                <input  id="password" type="password" class="input100  @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                <span class="focus-input100" data-placeholder="Your Password"></span>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="wrap-input100 validate-input">
                                <input id="password-confirm" type="password" class="input100" name="password_confirmation" required autocomplete="new-password">
                                <span class="focus-input100" data-placeholder="Re-Type Your Password"></span>

                            </div>

                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                               <div class="login100-form-bgbtn"></div>
                                <button type="submit" class="login100-form-btn">
                                    {{ __('sign up') }}
                                </button>
                            </div>
                        </div >
                        <a style=" padding: 35px;" class="txt2" href="{{url('/login')}}">
                            <h4 style="text-align: center;">Login</h4>
                        </a>
                </form>


        </div>
    </section>
</div>


</body>
</html>
