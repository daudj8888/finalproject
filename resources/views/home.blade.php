<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">


</head>

<body>


  @guest
  @if (Route::has('login'))
      <li class="nav-item">
          <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
      </li>
  @endif
  {{-- <a class="nav-link" href="{{ route('post.create') }}">Create Post</a> --}}

  @if (Route::has('register'))
      <li class="nav-item">
          <a class="nav-link" href="{{ route('register') }}">{{ __('signup') }}</a>
      </li>
  @endif
@else
  <li class="nav-item dropdown">
      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
          {{ Auth::user()->name }}
      </a>

      <div >
          <form id="logout-form" action="{{ url('logout') }}" method="POST">
              {{ csrf_field() }}
      <button type="submit">Logout</button>
  </form>
  <a class="nav-link" href="{{ route('post.create') }}">Create Post</a>

@endguest






























</html>
